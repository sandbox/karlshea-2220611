<?php
/**
 * @file
 * Token callbacks for the CTools Context Token module.
 */

/**
 * Implements hook_token_info().
 */
function ctools_context_token_token_info() {
  // New token type for panel context
  $info['types']['ctools_context'] = array(
    'name' => t('CTools Context'),
    'description' => t('Exposes current CTools Contexts as tokens'),
  );

  $info['tokens']['ctools_context']['substitution'] = array(
    'name' => t('Context substitution'),
    'description' => t('A context keyword substitution'),
    'dynamic' => TRUE,
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function ctools_context_token_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();
  $sanitize = !empty($options['sanitize']);

  // Check the type
  if($type == 'ctools_context') {

    // TODO: can I get the current contexts without using Panels?

    // Get the current page display
    $display = panels_get_current_page_display();
    if($display) {

      // Get the substitution tokens
      $substitutions = token_find_with_prefix($tokens, 'substitution');

      // Loop through each found keyword token and try replacing it with the ctools context keyword substitution
      foreach ($substitutions as $keyword => $original) {
        $replacement = ctools_context_keyword_substitute($keyword, array(), $display->context);
        $replacements[$original] = $sanitize ? check_plain($replacement) : $replacement;
      }
    }
  }

  return $replacements;
}
